#!/bin/bash

cp node_modules/react-scripts/.eslintrc .
cp node_modules/react-scripts/.babelrc .

yarn add -D babel-core@6.17.0 babel-eslint@7.1.1 eslint@3.8.1 eslint-config-react-app@^0.5.0 babel-cli
