import React, { Component } from 'react';
import GoogleMap from './GoogleMap'
import Marker from './Marker'
import './App.css';

class App extends Component {
  state = { markers: [] }

  render() {
    return (
      <div className="App" style={{ height: '400px', width: '100%' }}>
        <button onClick={this.changeMarker}>Marker</button>
        <GoogleMap
          zoom={10}
        >
          {this.renderMarkers()}
          <span>foo</span>
        </GoogleMap>
      </div>
    );
  }

  componentDidMount() {
    this.setState({
      markers: [
        <Marker key='marker-0' position={{ lat: 37.4419, lng: -122.143 }} />,
        <Marker key='marker-1' position={{ lat: 37.4410, lng: -122.121 }} />,
      ]
    })
  }

  changeMarker = (e) => {
    e.preventDefault();
    const markers = this.state.markers;
    if (Math.random() < 0.5 && markers.length > 0) {
      console.log('remove');
      this.setState({
        markers: markers.slice(0, markers.length - 1)
      });
    } else {
      console.log('add');
      this.setState({
        markers: markers.concat([
          <Marker key={`marker-${Math.random()}`} position={{ lat: 37.4419 + Math.random() * 0.1, lng: -122.143 + 0.1 * Math.random() }} />
        ])
      });
    }
  }

  renderMarkers = () => {
    return this.state.markers;
  }
}

export default App;
