const windowCallBackFunc = '__googleMapInit__'

let instance = null;
let apiKey = null;

export const setApiKey = (key) => {
  apiKey = key;
}

const initMap = (cb) => {
  return () => {
    delete window[windowCallBackFunc];
    if (typeof cb === 'function') {
      cb(window.google.maps);
    }
  }
}

const googleMapLoader = (...args) => {
  if (instance) {
    return instance;
  }

  let opts = {}
  if (args.length > 0) {
    opts = args[0];
  }

  const key = opts.key || apiKey || process.env.GOOGLE_MAPS_API_KEY;

  instance = new Promise((resolve) => {
    window[windowCallBackFunc] = initMap(resolve);
    const script = document.createElement('script');
    script.src = `https://maps.googleapis.com/maps/api/js?key=${key}&callback=${windowCallBackFunc}`;
    script.async = true;
    document.body.appendChild(script);
  });

  return instance;
}

export default googleMapLoader;
