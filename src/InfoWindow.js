import PropTypes from 'prop-types';
import GoogleMapComponent from './GoogleMapComponent';

class InfoWindow extends GoogleMapComponent {
  static isMapElement = true;

  static propTypes = {
    ...GoogleMapComponent.propTypes,
    marker: PropTypes.object.isRequired,
    content: PropTypes.string,
    listeners: PropTypes.object
  };

  buildComponent = (props) => {
    const infoWindow =  new this.googleMaps.InfoWindow({
      ...this.getComponentOptions(props)
    });

    infoWindow.open(props.map, props.marker)

    return infoWindow
  }
}

export default InfoWindow;
