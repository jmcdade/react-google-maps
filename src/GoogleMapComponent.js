import React from 'react';
import isEqual from 'lodash/isEqual';
import omit from 'lodash/omit';

class GoogleMapComponent extends React.Component {
  static propTypes = {
  }

  state = {
    cmp: null
  }

  get googleMaps() {
    return this.props.googleMaps;
  }

  get map() {
    return this.props.map;
  }

  render() {
    return null;
  }

  componentDidMount() {
    return this.renderCmp(this.props)
  };

  shouldComponentUpdate(nextProps) {
    if (!this.props.map && nextProps.map) return true;
    const options = omit(this.props, ['map', 'googleMaps']);
    const nextOptions = omit(nextProps, ['map', 'googleMaps']);
    return !isEqual(options, nextOptions);
  };

  componentDidUpdate(nextProps) {
    return this.renderCmp(nextProps)
  };

  renderCmp = (props) => {
    if (!props.map) return;

    if (!this.buildComponent) {
      console.error(`Missing buildComponent function in ${this}`);
      return;
    }

    let cmp = this.state.cmp
    if (!cmp) {
      cmp = this.buildComponent(props);
      this.setState({ cmp });
    } else {
      const options = this.getComponentOptions(props);
      cmp.setValues(options);
    }

    if (this.props.listeners) {
      Object.keys(this.props.listeners).forEach((key) => {
        this.googleMaps.event.clearListeners(cmp, key);
        const fn = this.props.listeners[key];
        cmp.addListener(key, fn);
      });
    }
  }

  getComponentOptions (props) {
    return omit({
      ...props,
    }, ['map', 'googleMaps', 'listeners']);
  };

  componentWillUnmount() {
    if (this.state.cmp) {
      this.state.cmp.setMap(null);
    }
  }
}

export default GoogleMapComponent;
