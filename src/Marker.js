import PropTypes from 'prop-types';
import GoogleMapComponent from './GoogleMapComponent';

import { latlng } from './types'

class Marker extends GoogleMapComponent {
  static isMapElement = true;

  static propTypes = {
    ...GoogleMapComponent.propTypes,
    position: latlng.isRequired,
    clickable: PropTypes.bool,
    listeners: PropTypes.object
  };

  buildComponent = (props) => {
    return new this.googleMaps.Marker({
      map: props.map,
      ...this.getComponentOptions(props)
    });
  }

  getComponentOptions (props) {
    const options = super.getComponentOptions(props);
    return { ...options, position: props.position };
  };
}

export default Marker;
