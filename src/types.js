import PropTypes from 'prop-types';

export const latlng = PropTypes.shape({ lat: PropTypes.number, lng: PropTypes.number });
