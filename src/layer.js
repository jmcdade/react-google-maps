import GoogleMapComponent from './GoogleMapComponent';

export default function layer (layerName) {
  return class Layer extends GoogleMapComponent {
    static isMapElement = true;

    buildComponent = (props) => {
      const options = this.getComponentOptions(props);
      return new this.googleMaps[layerName]({
        ...options,
        map: props.map
      });
    }
  }
}
