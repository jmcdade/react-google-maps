import PropTypes from 'prop-types';
import GoogleMapComponent from './GoogleMapComponent';

class DirectionsRenderer extends GoogleMapComponent {
  static isMapElement = true;

  static propTypes = {
    ...GoogleMapComponent.propTypes,
    directions: PropTypes.object.isRequired
  };

  buildComponent = (props) => {
    return new this.googleMaps.DirectionsRenderer({
      map: props.map,
      ...this.getComponentOptions(props)
    });
  }
}

export default DirectionsRenderer;
