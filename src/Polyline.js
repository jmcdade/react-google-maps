import PropTypes from 'prop-types';
import GoogleMapComponent from './GoogleMapComponent';

class Polyline extends GoogleMapComponent {
  static isMapElement = true;

  static propTypes = {
    ...GoogleMapComponent.propTypes,
    icons: PropTypes.array,
    path: PropTypes.array,
    strokeColor: PropTypes.string,
    strokeOpacity: PropTypes.number,
    strokeWeight: PropTypes.number,
    strokeWeight: PropTypes.number
  };

  buildComponent = (props) => {
    return new this.googleMaps.Polyline({
      map: props.map,
      ...this.getComponentOptions(props)
    });
  }

  getComponentOptions (props) {
    const options = super.getComponentOptions(props);
    return { ...options };
  };
}

export default Polyline;
