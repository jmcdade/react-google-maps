import omit from 'lodash/omit';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactDOM from 'react-dom';

import { latlng } from './types'
import googleMapLoader, { setApiKey } from './googleMapLoader';

const controlledProps = [
  'center',
  'zoom',
  'layerTypes',
  'mapTypeControl',
  'streetViewControl',
  'scaleControl',
  'rotateControl',
  'clickableIcons',
  'fullscreenControl',
  'mapTypeId',
  'styles'
];

class GoogleMap extends Component {
  static defaultProps = {
    center: { lat: 37.4419, lng: -122.143 },
    zoom: 11
  }

  static propTypes = {
    center: latlng.isRequired,
    zoom: PropTypes.number,
    layerTypes: PropTypes.array,
    mapTypeControl: PropTypes.bool,
    streetViewControl: PropTypes.bool,
    scaleControl: PropTypes.bool,
    rotateControl: PropTypes.bool,
    clickableIcons: PropTypes.bool,
    fullscreenControl: PropTypes.bool,
    mapTypeId: PropTypes.string,
    styles: PropTypes.object,
    options: PropTypes.object,
    listeners: PropTypes.shape({
      click: PropTypes.oneOfType([PropTypes.func, PropTypes.arrayOf(PropTypes.func)])
    })
  }

  state = { googleMaps: null }

  get googleMaps() {
    return this.state.googleMaps;
  }

  componentWillMount() {
    if (!this.state.googleMaps) {
      googleMapLoader().then(googleMaps => {
        this.setState({ googleMaps });
      });
    }
  }

  componentDidUpdate() {
    if (this.state.map) {
      const options = omit(this.getOptions(this.props), 'zoom', 'center');
      this.state.map.setOptions(options);
    }

    if (!this.googleMaps || this.state.map) return;

    const { Map } = this.googleMaps;
    const container = ReactDOM.findDOMNode(this);

    const options = this.getOptions(this.props);
    const map = new Map(container, options)
    this.setState({ map, options });

    if (this.props.listeners) {
      Object.keys(this.props.listeners).forEach((eventName) => {
        const handler = this.props.listeners[eventName];
        this.googleMaps.event.clearListeners(map, 'click');
        map.addListener(eventName, handler);
      });
    }
  }

  render() {
    const styles={ height: '100%' };
    return (
      <div style={styles}>
        {this.getChildren()}
      </div>
    );
  }

  getChildren = () => {
    if (!this.props.children || !this.state.map) return null;

    const children = React.Children.map(this.props.children, (child) => {
      if (!child) return null;

      if (child.type.isMapElement) {
        return React.cloneElement(child, {
          googleMaps: this.googleMaps,
          map: this.state.map
        });
      } else {
        return child;
      }
    });

    return React.Children.toArray(children);
  }

  getOptions = (props) => {
    const options = {};

    controlledProps.forEach(field => {
      if (props[field] !== null && typeof props[field] !== 'undefined') {
        options[field] = props[field];
      }
    });

    return {
      ...options,
      ...this.props.options
    };
  }
}

GoogleMap.setApiKey = setApiKey;
GoogleMap.googleMapLoader = googleMapLoader;

export default GoogleMap;
